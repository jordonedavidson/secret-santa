# Secret Santa

**A simple react-based Secret Santa picker.**

## How to start

Clone this project then follow the directions in the [Node Playground instructions](NODE-PD.md) to get up and running.

## Next Steps

You can run the project locally and just use it. Or you can run the **build** command to create a folder that can be pushed to your hosting site of choice.
