import React, { ReactEventHandler, useState } from "react"


export const App: React.FC = () => {
    const [santas, setSantas] = useState([])
    const [results, setResults] = useState([])

    const addSanta = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
        e.preventDefault()
        const santa = document.getElementById('new-santa') as HTMLInputElement

        // make sure that name isn't arlready in the list.
        if (santas.includes(santa.value)) {
            alert(`There is already someone named "${santa.value}" in the list of Santas. Please choose a different name to avoid confusion`)
            return
        }
        setSantas([...santas, santa.value])
        santa.value = ""
    }

    const removeSanta = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
        e.preventDefault()
        const button = e.target as HTMLButtonElement

        const id = parseInt(button.dataset.santa_id)

        const newSantas = [...santas]
        
        newSantas.splice(id, 1)

        setSantas(newSantas)
    }

    const selectSantas = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
        if (santas.length <=2) {
            alert("You need at least 3 santas.")
            return
        }

        // placeholder array for sorting.
        const tempResults = []

        // shuffle the recievers.
        let shuffled = shuffleArray(santas)
        
        for (let i = 0; i < shuffled.length; i++) {
            // match each member of the shuffled array with the next member.
            // the last member will give to the first.
            tempResults.push([shuffled[i], shuffled[(i+1) % shuffled.length]])
        }

        // Sort the array alphabetically to obscure the methodology.
        tempResults.sort((a, b) => {
           return  (b[0] > a[0]) ? -1 : 1
        })
        
        setResults(tempResults)
    }

    function shuffleArray(deck: string[]): string[] {
        let shuffled = deck
        .map(value => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value)
        console.log(shuffled)
        return shuffled
    }

    return(
        <>
            <h1>Setup Your Santas</h1>
            <form className="border mt-3 p-5 row">
                <div className="col-6 mb-3">
                    <input type="text" id="new-santa" className="form-control" />
                </div>
                <div className="col-3 mb-3">
                    <button className="btn btn-primary" onClick={addSanta}>Add Santa <i className="bi bi-tree"></i></button>
                </div>
            </form>
            <div className="mt-3">
                <h2>Santas</h2>
                {
                    santas.map((santa: string, i: number)=> {
                        const bg = (i % 2 == 0) ? 'bg-stripe' : ''
                        return(
                            <div className={`row mb-1 ${bg} rounded`} key={i}>
                                <div className="col-6 py-2">{santa}</div>
                                <div className="col-3">
                                    <button className="btn btn-danger" data-santa_id={i} onClick={removeSanta}>
                                        <i className="bi bi-trash" data-santa_id={i}></i>
                                    </button>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
            <div className="mt-3">
                <button className="btn btn-success" onClick={selectSantas}>Select Santas <i className="bi bi-stars"></i></button>
            </div>
            <div className="row mt-3" id="results">
                {
                    results.map((result, i) => {
                        return(
                            <div className="row mb-1" key={i}>
                                <p>{result[0]} gives to {result[1]}</p>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
}