export function patternExists(arr: Array<string[]>, a: string, b: string): boolean {
    let result = false
    arr.forEach((k) => {
        if (k[0] === a && k[1] === b) { result = true }
    })

    return result
}

export function isAReceiver(arr: Array<string[]>, a: string): boolean {
    let result = false

    arr.forEach((k) => {
        if (k[1] === a) { result = true }
    })

    return result
}

export function getGiver(arr: Array<string[]>, a: string): string | null {
    let result = null;

    arr.forEach((k) => {
        if (k[1] === a) { result = k[0] }
    })

    return result
}